import { Component, OnInit } from '@angular/core';
import { RickAndMortyApiService } from 'src/app/services/rick-and-morty-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IResponseCharacterApi } from 'src/app/models/response-character-interface';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.css']
})
export class CharacterDetailComponent implements OnInit {
  public infoCharacter: IResponseCharacterApi | undefined;

  constructor(private rickAndMortyApiService: RickAndMortyApiService,
    private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.getData(parseInt(id ? id : '1'));
  }

  getData(id: Number | undefined) {
    this.rickAndMortyApiService.getCharacter(id).subscribe(
      (response) => {
        console.log(response);
        this.infoCharacter = response;
      }
    );
  }

  goBack(){
    this.router.navigate(['list']);
  }

}
