import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { IResponseCharacterApi } from 'src/app/models/response-character-interface';
import { IResponseInformationApi } from 'src/app/models/response-information-interface';
import { RickAndMortyApiService } from 'src/app/services/rick-and-morty-api.service';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list-component.html',
  styleUrls: ['./character-list-component.css']
})
export class CharacterListComponent implements OnInit {
  public infoApi: IResponseInformationApi | undefined;
  public data: Array<IResponseCharacterApi> | undefined;

  constructor(private rickAndMortyApiService: RickAndMortyApiService) {
  }

  ngOnInit(): void {
    this.getData(1);
  }

  pageEvent($event: PageEvent) {
    console.log($event);
    this.getData($event.pageIndex + 1);
  }

  getData(page: number) {
    this.rickAndMortyApiService.getListCharacters(page).subscribe(
      (response) => {
        console.log(response);
        if (!this.infoApi) {
          this.infoApi = response.info;
        }
        this.data = response.results;
      }
    );
  }

}
