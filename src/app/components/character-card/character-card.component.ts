import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { IResponseCharacterApi } from 'src/app/models/response-character-interface';

@Component({
  selector: 'app-character-card',
  templateUrl: './character-card.component.html',
  styleUrls: ['./character-card.component.css']
})
export class CharacterCardComponent{
  @Input() public infoCharacter: IResponseCharacterApi | undefined;

  constructor(private router: Router) {
  }

  goToDetail(){
    this.router.navigate(['/detail', this.infoCharacter?.id]);
  }

}
