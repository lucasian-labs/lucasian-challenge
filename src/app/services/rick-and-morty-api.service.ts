import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IResponseApi } from '../models/response-api-interface';
import { Observable } from 'rxjs/internal/Observable';
import { IResponseCharacterApi } from '../models/response-character-interface';

@Injectable({
  providedIn: 'root',
})
export class RickAndMortyApiService {
  private URL_CHARACTERS: string;

  constructor(private http: HttpClient) {
    this.URL_CHARACTERS = "https://rickandmortyapi.com/api/character";
  }

  getListCharacters(page: Number = 1): Observable<IResponseApi>{
    const url = `${this.URL_CHARACTERS}/?page=${page}`
    return this.http.get<IResponseApi>(url);
  }

  getCharacter(id: Number = 1): Observable<IResponseCharacterApi> {
    const url = `${this.URL_CHARACTERS}/${id}`
    return this.http.get<IResponseCharacterApi>(url);
  }

}