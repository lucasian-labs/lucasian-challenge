import { IResponseCharacterApi } from "./response-character-interface";
import { IResponseInformationApi } from "./response-information-interface";

export interface IResponseApi {
  info: IResponseInformationApi;
  results: Array<IResponseCharacterApi>;
}