import { IResponseCharacterOriginApi } from "./response-character-origin-interface";

export interface IResponseCharacterApi {
  id: Number;
  name: String;
  status: String;
  species: String;
  type: String;
  gender: String;
  image: String;
  origin: IResponseCharacterOriginApi;
}