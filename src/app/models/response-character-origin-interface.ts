export interface IResponseCharacterOriginApi {
  name: String;
  status: String;
  url: String;
}