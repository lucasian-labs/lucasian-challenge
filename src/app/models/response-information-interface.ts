export interface IResponseInformationApi {
  count: Number;
  pages: Number;
  next: String;
  prev: String;
}